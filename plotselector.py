# Select between multiple strings of keywords
# combine methods or combine with standard datasets
# Map conflicts, love, whatever
# Predict whether sequence is likely to be in dataset -> more likely -> better fit

from nltk.sentiment.vader import SentimentIntensityAnalyzer
import nltk
import requests
# from transformers import pipeline
import re
import random as r
from gensim.models import KeyedVectors

# Load vectors directly from the file
model = KeyedVectors.load_word2vec_format('data/GoogleNews-vectors-negative300.bin', binary=True)
word_vectors = model.wv


# Access vectors for specific words with a keyed lookup:
# vector = model['easy']
# see the shape of the vector (300,)
# vector.shape
# Processing sentences is not as simple as with Spacy:
# vectors = [model[x] for x in "This is some text I am processing with Spacy".split(' ')]

# nltk.download('vader_lexicon')

def get_conceptnet_data_word(word):
    return get_conceptnet_data(word, "", 100)


def get_conceptnet_data(word, relation, limit):
    if not isinstance(word, str):
        try:
            word = str(word)
        except:
            return ''
    if relation == '':
        url = 'http://api.conceptnet.io/query?start=/c/en/' + word + '&limit=' + str(limit)
        obj = requests.get(url).json()
    else:
        url = 'http://api.conceptnet.io/query?start=/c/en/' + word + '&rel=/r/' + relation + '&limit=' + str(limit)
        obj = requests.get(url).json()
    return obj


def get_valence(word):
    sid = SentimentIntensityAnalyzer()
    return sid.polarity_scores(word)


def select_priorities(keyword, sentence_valence, relation_type=' '):
    # Optional relationship type
    # Check whether location is present in query, else:
    cn_edges = get_conceptnet_data(keyword, relation_type, 100)
    if len(cn_edges.get('edges', 0)) < 1:
        return ''
    print(cn_edges)
    # keys = [x['end']['term'].rsplit('/', 1)[-1] for x in cn_edges['edges']]
    keys = [(x['surfaceText'], x['end']['term'].rsplit('/', 1)[-1]) for x in cn_edges['edges']]
    print(keys)
    # selected_key = select_edge_from_valence(keys, sentence_valence)
    selected_key = select_edge_from_valence_surface(keys, sentence_valence)
    return selected_key


def select_edge_from_valence(cn_data_labels, sentence_valence):
    # Init valence far away
    best_valence = 10
    best_key = 'none'
    for word in cn_data_labels:
        word = re.sub(r'[^\w\s]', '', word)
        # print(word)
        valence = float(get_valence(word).get('compound', 0))
        # valence = get_valence_alt(word)
        # print(valence)
        if abs(sentence_valence - valence) < abs(sentence_valence - best_valence):
            best_valence = valence
            best_key = word
    return best_key


def select_edge_from_valence_surface(cn_data_labels, sentence_valence):
    # Init valence far away
    best_valence = 10
    best_key = 'none'
    for (surface, word) in cn_data_labels:
        if surface is None:
            continue
        token = re.sub(r'[^\w\s]', '', word)
        surface = re.sub(r'[^\w\s]', '', surface)
        # print(word)
        valence = float(get_valence(surface).get('compound', 0))
        # valence = get_valence_alt(word)
        # print(valence)
        if abs(sentence_valence - valence) < abs(sentence_valence - best_valence):
            best_valence = valence
            best_key = token
    return best_key


def fill_keywords(keyword, sentence_valence):
    sentence_valence = float(sentence_valence)
    keyword = re.sub(r'[^\w\s]', '', keyword)
    location = select_priorities(keyword, sentence_valence, relation_type='AtLocation')
    person = select_priorities(keyword, sentence_valence,
                               relation_type='CreatedBy')
    # TODO get subject from input sentence, conceptnet does not really offer anything
    effect = select_priorities(keyword, sentence_valence, relation_type='Causes')
    cause = select_priorities(keyword, sentence_valence, relation_type='ReceivesAction')
    motivation = select_priorities(keyword, sentence_valence, relation_type='MotivatedByGoal')
    challenge = select_priorities(keyword, sentence_valence, relation_type='ObstructedBy')
    desire = select_priorities(keyword, sentence_valence, relation_type='Desires')
    creator = select_priorities(keyword, sentence_valence, relation_type='CreatedBy')
    # Return all found values
    words = [x for x in [location, person, effect, cause, motivation, challenge, desire, creator] if x != 0]
    return words


def generate_plot(keywords, sentence_valence=1):
    # TODO lemmatize verb and noun, sentence
    if 'nsubj' in keywords:
        if isinstance(keywords['nsubj'], list):
            keywords['nsubj'] = keywords['nsubj'][0]
    if 'nsubj' not in keywords:
        random_value = r.choice([item for elem in keywords.values() for item in elem])
        if isinstance(random_value, str):
            keywords['nsubj'] = select_priorities(random_value, sentence_valence,
                                                  relation_type='CreatedBy')
    if 'nsubj' not in keywords:
        keywords['nsubj'] = 'Henkie'
    plot_words = []
    if 'location' not in keywords:
        location = select_priorities(keywords['nsubj'], sentence_valence, relation_type='AtLocation')
        plot_words.append(location)
    else:
        plot_words.append(keywords['location'])
    plot_words.append(keywords['nsubj'])
    if 'verb' not in keywords:
        verb = select_priorities(keywords['nsubj'], sentence_valence, relation_type='UsedFor')
    else:
        verb = keywords['verb']
    plot_words.append(verb)
    if 'pobj' in keywords:
        if isinstance(keywords['pobj'], list):
            keywords['pobj'] = keywords['pobj'][0]
        pobj = keywords['pobj']
    elif 'dobj' in keywords:
        if isinstance(keywords['dobj'], list):
            keywords['dobj'] = keywords['dobj'][0]
        pobj = keywords['dobj']
    else:
        pobj = select_priorities(keywords['nsubj'], sentence_valence, relation_type='UsedFor')
    effect = select_priorities(verb, sentence_valence, relation_type='Causes')
    if not effect:
        effect = select_priorities(pobj, sentence_valence, relation_type='Causes')
    plot_words.append(effect)
    cause = select_priorities(verb, sentence_valence, relation_type='ReceivesAction')
    if not cause:
        cause = select_priorities(pobj, sentence_valence, relation_type='ReceivesAction')
    plot_words.append(cause)
    motivation = select_priorities(verb, sentence_valence, relation_type='MotivatedByGoal')
    if not motivation:
        motivation = select_priorities(pobj, sentence_valence, relation_type='MotivatedByGoal')
    plot_words.append(motivation)
    plot_words = [x for x in plot_words if x != '']
    print("Length: {length}, Content: {content}".format(length=len(plot_words), content=plot_words))
    # challenge = select_priorities(keyword, sentence_valence, relation_type='ObstructedBy')
    # desire = select_priorities(keyword, sentence_valence, relation_type='Desires')
    # creator = select_priorities(keyword, sentence_valence, relation_type='CreatedBy')
    return plot_words


def find_word(keyword, relation_type=''):
    cn_location_edges = get_conceptnet_data_word(keyword)
    if len(cn_location_edges.get('edges', 0)) < 1:
        return ''
    # print(cn_location_edges)

    keys = []
    for x in cn_location_edges['edges']:
        ids = x['@id'].rsplit(',')
        # print(ids)
        relation = ids[0].rsplit('/')[-2]
        if relation_type != '':
            if relation == relation_type:
                for id in ids[1:]:
                    new_id = re.sub('^/c/en/', '', id).split('/')[0]
                    if new_id != keyword:
                        keys.append(new_id)
        elif relation != 'Synonym':
            for id in ids[1:]:
                new_id = re.sub('^/c/en/', '', id).split('/')[0]
                if new_id != keyword:
                    keys.append(new_id)
    print("keyword: ", keyword)
    print("KEYS: ", keys)

    # Select a similar word based on word embedding similarity
    similarity = {}
    selected_keys = []
    selected_key = ""
    for key in keys:
        if key in word_vectors.vocab and key != '':
            similarity[key] = model.similarity(key, keyword)
            # print(key, keyword, model.similarity(key, keyword))
    if len(similarity) != 0:
        avg = sum(similarity.values()) / len(similarity)
        for key in similarity:
            if similarity[key] > avg:
                selected_keys.append(key)
    if len(selected_keys) != 0:
        selected_key = r.choice(selected_keys)
    return selected_key


def generate_plot_danielle(keywords):
    plot_words = []
    # make sure the subject is filled in
    if 'nsubj' in keywords.keys():
        if isinstance(keywords['nsubj'], list):
            keywords['nsubj'] = keywords['nsubj'][0]
            if isinstance(keywords['nsubj'], list):
                keywords['nsubj'] = keywords['nsubj'][0]
    if 'nsubj' not in keywords.keys():
        random_value = r.choice(list(keywords.values()))
        if isinstance(random_value, str):
            keywords['nsubj'] = find_word(random_value, relation_type='')

    # get location of subject
    if 'location' not in keywords.keys():
        location = find_word(keywords['nsubj'], relation_type='AtLocation')
        plot_words.append(location)
    else:
        plot_words.append(keywords['location'])
    plot_words.append(keywords['nsubj'])

    if 'verb' not in keywords.keys():
        keywords['verb'] = find_word(keywords['nsubj'], relation_type='UsedFor')

    verb = find_word(keywords['verb'], relation_type='MannerOf')
    if verb == "":
        verb = find_word(keywords['verb'], relation_type='IsA')
    print("VERB", verb)
    plot_words.append(verb)

    other_words = []
    types = ['pobj', 'dobj', 'valence']
    result = []
    for type in types:
        if type in keywords.keys():
            if isinstance(keywords[type], list):
                for i in keywords[type]:
                    other_words.append(i)
            else:
                other_words.append(keywords[type])
    other_words = list(dict.fromkeys(other_words))  # remove duplicates
    print("other words: ", other_words)
    for word in other_words:
        plot_words.append(find_word(word, relation_type=''))

    plot_words = [x for x in plot_words if x != '']
    print("Length: {length}, Content: {content}".format(length=len(plot_words), content=plot_words))
    return plot_words


def generate_plot_jelle(keywords, sentence_valence=1):
    plot_words = []
    print(sentence_valence)
    # make sure the subject is filled in
    if 'nsubj' in keywords.keys():
        if isinstance(keywords['nsubj'], list):
            keywords['nsubj'] = keywords['nsubj'][0]
            if isinstance(keywords['nsubj'], list):
                keywords['nsubj'] = keywords['nsubj'][0]
    if 'nsubj' not in keywords.keys():
        random_value = r.choice(list(keywords.values()))
        if isinstance(random_value, str):
            keywords['nsubj'] = select_priorities(random_value, sentence_valence, relation_type='')

    # get location of subject
    if 'location' not in keywords.keys():
        location = select_priorities(keywords['nsubj'], sentence_valence, relation_type='AtLocation')
        plot_words.append(location)
    else:
        plot_words.append(keywords['location'])
    plot_words.append(keywords['nsubj'])

    if 'verb' not in keywords.keys():
        keywords['verb'] = select_priorities(keywords['nsubj'], sentence_valence, relation_type='UsedFor')

    verb = select_priorities(keywords['verb'], sentence_valence, relation_type='MannerOf')
    if verb == "":
        verb = select_priorities(keywords['verb'], sentence_valence, relation_type='IsA')
    print("VERB", verb)
    plot_words.append(verb)

    other_words = []
    types = ['pobj', 'dobj', 'valence']
    result = []
    for type in types:
        if type in keywords.keys():
            if isinstance(keywords[type], list):
                for i in keywords[type]:
                    other_words.append(i)
            else:
                other_words.append(keywords[type])
    other_words = list(dict.fromkeys(other_words))  # remove duplicates
    print("other words: ", other_words)
    for word in other_words:
        plot_words.append(select_priorities(word, sentence_valence, relation_type=''))

    plot_words = [x for x in plot_words if x != '']
    print("Length: {length}, Content: {content}".format(length=len(plot_words), content=plot_words))
    return plot_words


""" else:
     pobj = select_priorities(keywords['nsubj'], sentence_valence, relation_type='UsedFor')
 effect = select_priorities(verb, sentence_valence, relation_type='Causes')
 if not effect:
     effect = select_priorities(pobj, sentence_valence, relation_type='Causes')
 plot_words.append(effect)
 cause = select_priorities(verb, sentence_valence, relation_type='ReceivesAction')
 if not cause:
     cause = select_priorities(pobj, sentence_valence, relation_type='ReceivesAction')
 plot_words.append(cause)
 motivation = select_priorities(verb, sentence_valence, relation_type='MotivatedByGoal')
 if not motivation:
     motivation = select_priorities(pobj, sentence_valence, relation_type='MotivatedByGoal')
 plot_words.append(motivation)
 """
