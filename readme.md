
# README

And that is how... you automatically generate storylines based on the concluding sentence.

This repo contains the code for the Advanced project of Natural Language Processing course at the University of Twente. 
This project is made by J.G.Hamoen, D.J. Kwakkel, and M. Keurhorst. The code takes conclusion sentences from 'starts.csv', and generates keywords leading up to said conclusion. There are two methods which are used: Valence selection and word similarity selection on conceptnet edges.

## Usage
* Install dependencies: 'pip3 install -r requirements.txt'
* Check the input sentences in 'start.csv', or use your own
* Run main.py
* The output can be found in 'output.txt'

The sentences used to generate keywords are drawn from 'starts.csv'. If you wish to use your own sentences, simply replace 'starts.csv' or drop in a different file and change the path in 'main.py'.