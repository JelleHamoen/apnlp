from plotselector import get_valence
from keyword_extraction import get_keywords


def parse_sentence_valence(sentence):
    obj = {'text': sentence, 'compound_valence': get_valence(sentence)['compound'],
           'raked_keywords': get_keywords(sentence)}
    print(obj['raked_keywords'])
    valence_words = [(i, get_valence(i)['compound']) for i in sentence.split()]
    valence_keywords = [(i, get_valence(i)['compound']) for i in obj['raked_keywords']]
    # Rank in order (least to highest)
    sorted_valence_words = sorted(valence_words, key=lambda tup: tup[1])
    sorted_valence_keywords = sorted(valence_keywords, key=lambda tup: tup[1])
    obj['sorted_valence_words'] = sorted_valence_words
    obj['sorted_valence_keywords'] = sorted_valence_keywords
    # Calculate compound valence of a sentence
    # print(sorted_valence_words)
    # print(sorted_valence_keywords)
    return obj
