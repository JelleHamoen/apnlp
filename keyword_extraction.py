from rake_nltk import Rake
from nltk import pos_tag
from plotselector import get_valence
import re
import spacy

nlp = spacy.load("en_core_web_sm")


def get_keywords_pos(text):
    returnlist = []
    words = text.split()
    tagged = pos_tag(words)
    for i in tagged:
        if ('NN' in i[1] or 'VB' in i[1]):
            returnlist.append(i[0])
    return returnlist


def get_keywords(text):
    text = re.sub(r'[^\w\s]', '', text)
    Dict = {}
    compound = []
    term = ""
    doc = nlp(text)
    print(doc)
    # CHECK WITH NER IF THERE'S A LOCATION GIVEN
    for ent in doc.ents:
        # print(ent.text, ent.start_char, ent.end_char, ent.label_)
        if ent.label_ == 'GPE':
            Dict['location'] = ent.text

    # CHECK FOR EVERY WORD IF IT IS A CERTAIN TYPE
    types = ['nsubj', 'ROOT', 'dobj', 'pobj']
    for index in range(len(doc)):
        token = doc[index]
        # print(token.text, token.dep_, token.head.text)
        for i in types:
            if i in token.dep_:
                if token.lemma_ != '-PRON-':
                    term = token.lemma_
                else:
                    term = token.text
                # CHECK IF THE CURRENT TOKEN IS PART OF A COMPOUND
                for j in range(len(compound)):
                    if token.text in compound[j]:
                        term = compound[j]
                # ADD TERM TO THE DICTIONARY
                if term != "":
                    if i not in Dict:
                        Dict[i] = term
                    else:
                        Dict[i] = [Dict[i]]
                        Dict[i].append(term)
        # SEE IF THERE'S A WORD WITH 'AT' WHICH SIGNALS A LOCATION
        # if token.head.text == 'at' or token.head.text == 'in' or token.head.text == 'on':
        if token.head.text == 'at':
            Dict['location'] = token.text
        # IF THE WORD IS THE FIRST PART OF A COMPOUND, SAVE IT
        if token.dep_ == 'compound':
            compound.append(token.text + " " + token.head.text)
            # print("COMPOUND", compound)

    # ADD VALENCE WORDS TO THE DICTIONARY IF IT'S NOT IN THERE YET
    raked = get_keywords_pos(text)
    for word in raked:
        if get_valence(word)['compound'] != 0:
            Dict['valence'] = word
    Dict['verb'] = Dict.pop('ROOT')  # RENAME ROOT TO VERB
    return Dict
