# This is the entry point of the application
import csv
import os
import requests
import keyword_extraction
# from valence_parsing import parse_sentence_valence
from plotselector import fill_keywords, get_valence, select_priorities, generate_plot_danielle, generate_plot, \
    generate_plot_jelle


def read_input_sentences(path):
    sentences = []
    with open(path, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in reader:
            sentences.append(" ".join(row))
    return sentences


inputs = read_input_sentences('starts.csv')
f = open("output.txt", "w")
f.write("'sentence','valence','similarity'")
for x in inputs:
    keywords = keyword_extraction.get_keywords(x)
    print('keywords : ', keywords)
    sentence_valence = get_valence(x).get('compound', 0)
    plot_words_j = generate_plot_jelle(keywords, sentence_valence=sentence_valence)
    plot_words_d = generate_plot_danielle(keywords)
    line = [x, ' '.join(plot_words_j), ' '.join(plot_words_d)]
    print(line)
    f.write('\n')
    f.write(",".join(line))

f.close()
